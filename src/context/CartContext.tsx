import React, {createContext, useState, ReactNode, useEffect, useMemo, useCallback} from "react";
import {Product, CartContextType} from "../assets/types/types"

export const CartContext = createContext<CartContextType | undefined >(undefined);

const readCartFromLocalStorage = () => {
    const savedCart = localStorage.getItem('cart');
    if(savedCart) {
        try {
            return JSON.parse(savedCart);
        }catch(error) {
            console.log("Error parsing saved cart from localStorage", error)
            return [];
        }
    }
}

const saveCartToLocalStorage = (cart : Product[]) => {
    localStorage.setItem('cart', JSON.stringify(cart))
}

export const CartProvider: React.FC<{children: ReactNode}> = ({ children }) => {
    const [cart, setCart] = useState<Product[]>(() => readCartFromLocalStorage())
    const [isCartOpen, setIsCartOpen] = useState<boolean>(false);

    // Save cart to localStorage whenever it changes
    useEffect(() => {
        saveCartToLocalStorage(cart)
    }, [cart]);

    const addToCart = (product: Product) => {
        setCart(prevCart => {
          const existingProduct = prevCart.find(item => item.id === product.id);
          if (existingProduct) {
            return prevCart.map(item =>
              item.id === product.id ? { ...item, quantity: item.quantity + 1 } : item
            );
          }
          return [...prevCart, { ...product, quantity: 1 }];
        });
      };
    
      const updateCartItem = (productId: number, delta: number) => {
        setCart(prevCart =>
          prevCart
            .map(item =>
              item.id === productId ? { ...item, quantity: item.quantity + delta } : item
            )
            .filter(item => item.quantity > 0)
        );
      };
        
    const openCartPage = useCallback(() => setIsCartOpen(prevState => !prevState), []);

    const contextValue = useMemo(() => ({
        cart,
        isCartOpen,
        addToCart,
        updateCartItem,
        openCartPage,
      }), [cart, isCartOpen, openCartPage]);

    return (
        <CartContext.Provider value={contextValue}>
            {children}
        </CartContext.Provider>
    )
}
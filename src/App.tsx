import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import HomePage from './Components/HomePage/HomePage';
import Category from './Components/Category/Category';
import Header from './Components/Header/Header';
import Footer from './Components/Footer/Footer';
import categories from './assets/data/categoryData';
import products from './assets/data/ProductData';
import Cart from './Components/Cart/Cart';
import { Category as CategoryType } from './assets/types/types';

const App:React.FC = () => {

  return (
      <Router>
        <Header categories={categories} />
        <div className='pt-[126px]'>
          <Routes>
            <Route path="/" element={<HomePage categories={categories} products={products} />} />
            {categories.map((category:CategoryType) => <Route key={category.id} path={`/${category.token}`} element={<Category category={category} products={products} />} />)}
          </Routes>
        </div>
        <Cart />
        <Footer />
      </Router>
  );
}

export default App;
import starterCat from '../img/startCat.jpg';
import dishCat from '../img/dishCat.jpg';
import dessertCat from '../img/dessertCat.jpg';
import { Category } from '../types/types';

const categories: Category[] = [
    { id: 1, token:'starters', title: 'Entrées', introduction: 'Découvrez nos entrées !', image: starterCat},
    { id: 2, token:'dishes', title: 'Plats', introduction: 'Découvrez nos plats !', image: dishCat },
    { id: 3, token:'desserts', title: 'Desserts', introduction: 'Découvrez nos desserts !', image: dessertCat },
  ];

export default categories
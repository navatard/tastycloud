import Oeufs from '../img/starters/oeufs.jpg';
import Soupe from '../img/starters/soupeoignons.jpg';
import Veloute from '../img/starters/veloute.jpg';
import Salade from '../img/starters/salade.jpg';
import Foie from '../img/starters/foiegras.jpg';
import Cake from '../img/starters/oeufs.jpg';
import Pate from '../img/starters/pate.jpg';
import Tartare from '../img/starters/oeufs.jpg';
import Croque from '../img/dishes/croque.jpg';
import Fondue from '../img/dishes/Fondue.jpg';
import Lasagnes from '../img/dishes/lasagnes.jpg';
import Noix from '../img/dishes/noix.jpg';
import Paela from '../img/dishes/paela.jpg';
import Poulet from '../img/dishes/poulet.jpg';
import Risotto from '../img/dishes/risotto.jpg';
import Tacos from '../img/dishes/tacos.jpg';
import Creme from '../img/desserts/creme.jpg';
import Dacquoise from '../img/desserts/dacquoise.jpg';
import Foret from '../img/desserts/foret.jpg';
import Ile from '../img/desserts/ile.jpg';
import Poire from '../img/desserts/poire.jpg';
import Pommes from '../img/desserts/pomme.jpg';
import Potiron from '../img/desserts/potiron.jpg';
import Yaourt from '../img/desserts/yaourt.jpg';
import { Product } from '../types/types';

const products: Product[] = [
    { id: 1, name: 'Oeufs à la coque', quantity:0, description: 'Pour réussir vos oeufs à la coque, suivez les 5 conseils suivants : Choisissez des oeufs extra frais et de taille similaire pour une cuisson homogène Sortez-les du réfrigérateur 1 heure avant de les cuire pour qu’ils soient à température ambiante', price: 4, image: Oeufs, category: 'starters' },
    { id: 2, name: "Soupe à l'oignon", quantity:0, description: "Faites-les revenir dans le mélange beurre et huile jusqu'à ce qu'ils soient tendres et légèrement dorés.", price: 6.50, image: Soupe, category: 'starters' },
    { id: 3, name: "Soupe veloutée de potimarron", quantity:0, description: "Enlever l'écorce et les pépins du potimarron (il n'est pas obligatoire de le peler, mais dans ce cas, le choisir bio et bien le laver) puis couper la chair en gros morceaux.", price: 5, image: Veloute, category: 'starters' },
    { id: 4, name: "Salade composée facile", quantity:0, description: "Ajouter sur la salade les dés de fromages, les tomates cerises, les dés de jambon, les olives noires et les œufs durs coupés en quartiers.", price: 10, image: Salade, category: 'starters' },
    { id: 5, name: "Fois Gras Maison", quantity:0, description: "Cette recette n'a qu'un but, c'est de désacraliser la préparation du foie gras maison.", price: 15, image: Foie, category: 'starters' },
    { id: 6, name: "Cake au thon", quantity:0, description: "Ajouter les oeufs et battez sans faire de grumeaux, ainsi que le lait, l'huile, le sel le poivre. Bien mélanger. Incorporer le thon en morceaux et le fromage râpé.", price: 4.99, image: Cake, category: 'starters' },
    { id: 7, name: "Pâté en croute", quantity:0, description: "Faire la pâte : mettre farine et beurre en morceaux dans un saladier, sabler. Ajouter une pincée de sel, l'eau pour obtenir une boule de pâte, à laisser reposer une demi-heure", price: 6.20, image: Pate, category: 'starters' },
    { id: 8, name: "Tartare de saumon", quantity:0, description: "Enlever la peau du filet de saumon, et le passer sous l'eau pour enlever d'éventuelles écailles.", price: 6, image: Tartare, category: 'starters' },
    { id: 9, name: "Poulet au four", quantity:0, description: "Préparer la sauce en mélangeant le jus de citron et le verre de bouillon de volaille. Arroser copieusement le poulet et verser le reste du jus dans le plat.", price: 21, image: Poulet, category: 'dishes' },
    { id: 10, name: "Risotto aux champignons", quantity:0, description: "Il existe sur la toile deux écoles du risotto aux champignons, à savoir celle du champignon qui cuit dans le riz, et celle des champignons poêlés ajoutés à la dernière minute.", price: 11, image: Risotto, category: 'dishes' },
    { id: 11, name: "Croque-monsieur", quantity:0, description: "Beurrez les 8 tranches de pain de mie sur une seule face. Posez 1 tranche de fromage sur chaque tranche de pain de mie.", price: 8.5, image: Croque, category: 'dishes' },
    { id: 12, name: "Fondue Savoyarde", quantity:0, description: "Dans un petit récipient, versez le vin blanc restant (5 cl), la Maïzena et la noix de muscade. Remuez et réservez.", price: 18.55, image: Fondue, category: 'dishes' },
    { id: 13, name: "Paëlla fruits de mer", quantity:0, description: "Découper le poulet en morceaux, nettoyer les moules, émincer le chorizo et les poivrons, peler et concasser les tomates, hacher les oignons et l'ail.", price: 20.55, image: Paela, category: 'dishes' },
    { id: 14, name: "Tacos mexicains", quantity:0, description: "Hors du feu, ajuster l'assaisonnement et saupoudrer généreusement de cumin; on peut aussi rajouter quelques gouttes de Tabasco.", price: 10.99, image: Tacos, category: 'dishes' },
    { id: 15, name: "Noix de Saint Jacques", quantity:0, description: "Faire fondre la noix de beurre dans une poêle. Déposer les noix et les faire dorer légèrement sur chaque côté.", price: 15.99, image: Noix, category: 'dishes' },
    { id: 16, name: "Lasagnes à la bolognaise", quantity:0, description: "Faire revenir gousses hachées d'ail et les oignons émincés dans un peu d'huile d'olive.", price: 17, image: Lasagnes, category: 'dishes' },
    { id: 17, name: "Crème dessert au chocolat", quantity:0, description: "Mélanger farine et beurre fondu comme une béchamel, ajouter le lait bien mélanger. Ajouter le sucre après la cuisson du lait dans le roux blanc, puis mélanger à nouveau.", price: 5, image: Creme, category: 'desserts' },
    { id: 18, name: "Yaourt à la banane", quantity:0, description: "Couper les bananes en rondelles et les mettre dans un plat. Y ajouter les yaourts nature. Bien mélanger.", price: 5, image: Yaourt, category: 'desserts' },
    { id: 19, name: "Crème dessert aux pommes", quantity:0, description: "Mettre le lait froid et la Maïzena dans un bocal, secouer énergiquement puis verser dans une casserole.", price: 4, image: Pommes, category: 'desserts' },
    { id: 20, name: "Dessert de potiron", quantity:0, description: "pelez et coupez le potiron en cube de 5 à 6 cm, disposez les dans une casserole assez large, ajoutez un verre d'eau, le beurre et laissez cuire à feu doux (ajoutez de l'eau si nécessaire)", price: 4, image: Potiron, category: 'desserts' },
    { id: 21, name: "Poires au miel ", quantity:0, description: "Peler les poires en leur laissant la queue et 1 cm de peau autour de celle-ci, pour pouvoir les prendre plus facilement.", price: 3, image: Poire, category: 'desserts' },
    { id: 22, name: "Forêt noire aux framboises ", quantity:0, description: "A l'aide d'un économe, râper le chocolat noir dans un grand récipient (pour éviter d'en mettre partout).", price: 3, image: Foret, category: 'desserts' },
    { id: 23, name: "Iles flotantes", quantity:0, description: "Laver et égoutter les fraises. Les mixer avec 2 cuillères à soupe de sucre, jusqu'à obtention d'un coulis.", price: 3, image: Ile, category: 'desserts' },
    { id: 24, name: "Dacquoise", quantity:0, description: "Monter les blancs en neige avec la larme de citron pressé et les sucres vanillé et semoule. Une fois la préparation bien ferme, incorporer délicatement aux blancs la poudre d'amandes mélangée au sucre glace.", price: 5.55, image: Dacquoise, category: 'desserts' },
  ];

export default products
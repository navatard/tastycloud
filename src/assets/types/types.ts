// types.ts
export interface Product {
    id: number;
    name: string;
    description: string;
    price: number;
    image: string;
    category: string;
    quantity: number;
  }
  
  export interface Category {
    id: number;
    title: string;
    introduction: string;
    token: string;
    image: string;
  }
  
  export interface CartContextType {
    cart: Product[];
    isCartOpen: boolean;
    openCartPage: () => void;
    addToCart: (product: Product) => void;
    updateCartItem: (productId: number, delta: number) => void;
  }
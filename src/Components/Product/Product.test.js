import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom';
import Product from './Product';
import { CartContext } from '../../context/CartContext';

// Mock data for the product
const product = {
  id: 1,
  name: 'Test Product',
  description: 'This is a test product description',
  price: 10,
  image: 'https://via.placeholder.com/150',
  category: 'test-category',
};

// Mock context values
const mockCartContext = {
  cart: [],
  addToCart: jest.fn(),
  updateCartItem: jest.fn(),
};

describe('Product Component', () => {
  test('renders product details correctly', () => {
    render(
      <CartContext.Provider value={mockCartContext}>
        <Product product={product} isInCart={false} />
      </CartContext.Provider>
    );

    // Check if the product name, description, and price are rendered correctly
    expect(screen.getByText('Test Product')).toBeInTheDocument();
    expect(screen.getByText('This is a test product description')).toBeInTheDocument();
    expect(screen.getByText('10€')).toBeInTheDocument();

    // Check if the add to cart button is present
    expect(screen.getByText('Ajouter')).toBeInTheDocument();
  });

  test('calls addToCart function when add button is clicked', () => {
    render(
      <CartContext.Provider value={mockCartContext}>
        <Product product={product} isInCart={false} />
      </CartContext.Provider>
    );

    const addButton = screen.getByText('Ajouter');
    fireEvent.click(addButton);

    expect(mockCartContext.addToCart).toHaveBeenCalledWith(product);
  });

  test('renders product quantity controls when product is in cart', () => {
    // Update the mock context to include the product in the cart
    mockCartContext.cart = [{ id: 1, quantity: 2 }];

    render(
      <CartContext.Provider value={mockCartContext}>
        <Product product={product} isInCart={true} />
      </CartContext.Provider>
    );

    // Check if the quantity controls are rendered
    expect(screen.getByText('2')).toBeInTheDocument();
  });

  test('calls updateCartItem function when quantity buttons are clicked', () => {
    mockCartContext.cart = [{ id: 1, quantity: 2 }];

    render(
      <CartContext.Provider value={mockCartContext}>
        <Product product={product} isInCart={true} />
      </CartContext.Provider>
    );

    const incrementButton = screen.getByText('+');
    const decrementButton = screen.getByText('-');

    fireEvent.click(incrementButton);
    expect(mockCartContext.updateCartItem).toHaveBeenCalledWith(1, 1);

    fireEvent.click(decrementButton);
    expect(mockCartContext.updateCartItem).toHaveBeenCalledWith(1, -1);
  });
});
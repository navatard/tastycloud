import React, { useContext } from 'react';
import { CartContext } from '../../context/CartContext';
import {Product as ProductType} from '../../assets/types/types'

interface ProductProps {
  product: ProductType;
  isInCart?: boolean
}

const Product:React.FC<ProductProps> = ({ product, isInCart = false }) => {

  const context = useContext(CartContext);

  if (!context) {
    throw new Error('CartContext must be used within a CartProvider');
  }

  const { cart, addToCart, updateCartItem } = context;

  const truncateText = (text: string, maxLength: number) => {
    if (text.length > maxLength) {
      return text.substring(0, maxLength) + '...';
    }
    return text;
  };

  // Trouver la quantité du produit dans le panier
  const productInCart = cart.find((item: ProductType) => item.id === product.id);
  const productQuantity = productInCart ? productInCart.quantity : 0;

  return (
    <article className={`${isInCart ? "w-full" : "w-1/4 first:pl-0"} p-5 `}>
      <div className='shadow-xl rounded-md overflow-hidden'>
        <img className="w-full h-36 object-cover" src={product.image} alt={product.name} />
        <div className='p-3'>
          <h3 className='font-semibold text-base'>{truncateText(product.name, 30)}</h3>
          <p className='text-sm py-2'>{truncateText(product.description, 80)}</p>
          <div className='flex justify-between items-center mt-3 h-[28px]'>
            <p className='font-semibold text-tasty'>{product.price}€</p>
            {productQuantity === 0 ? 
              <button className='py-1 px-4 bg-tasty text-white text-sm rounded-md' onClick={() => addToCart(product)}>Ajouter</button>
                :
              <div className='flex align-center'>
                <button className='bg-tasty text-white text-base rounded-md py-0 px-2' onClick={() => updateCartItem(product.id, -1)}>-</button>
                  <span className='px-2'>{productQuantity}</span>
                <button className='bg-tasty text-white text-base rounded-md py-0 px-2' onClick={() => updateCartItem(product.id, 1)}>+</button>
              </div>
            }
          </div>
        </div>
      </div>
    </article>
  );
}

export default Product;
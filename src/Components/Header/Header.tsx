import React, { useContext } from 'react';
import Logo from "../../assets/img/logo.jpg"
import Cart from "../../assets/img/ui/cart.png"
import {Link} from 'react-router-dom';
import { CartContext } from '../../context/CartContext';
import { Category, Product as ProductType } from '../../assets/types/types';

interface HeaderProps {
  categories: Category[]
}

const Header:React.FC<HeaderProps> = ({categories}) => {

  const context = useContext(CartContext);

  if (!context) {
    throw new Error('CartContext must be used within a CartProvider');
  }

  const { cart, openCartPage } = context;

  const totalQuantity = cart.reduce((acc: number, item: ProductType) => acc + item.quantity, 0);
  return (
    <header className='bg-tasty fixed w-full top-0 z-20'>
        <div className=" mx-40 flex justify-between items-center py-5">
            <div className="w-32">
                <img  src={Logo} alt='TastyCloud Logo' />
            </div>
            <div className='flex items-center cursor-pointer relative' onClick={() => openCartPage()}>
                <div className="w-10">
                    <img  src={Cart} alt='Cart Icon' />
                </div>
                <div className="absolute bottom-[10px] right-[23px]" >
                    <div className='relative'>
                    <span className='absolute text-tasty text-xs left-[5px] bg-white rounded-full p-0.5 w-[20px] h-[20px] text-center'>{totalQuantity}</span>
                    </div>
                </div>
            </div>
        </div>

        <nav className='bg-white w-full py-3 text-tasty shadow-md'>
          <ul className='flex mx-40'>
            <li className='pr-6'><Link to="/">Accueil</Link></li>
            {categories.map(({id, token, title}: Category) => <li key={`header-${id}`} className='pr-6'><Link to={`/${token}`}>{title}</Link></li>)}
          </ul>
        </nav>
    </header>
  );
}

export default Header;
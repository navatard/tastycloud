import React from 'react';
import Product from '../Product/Product';
import Heading from '../ui/Heading/Heading';
import HomeBackground from '../../assets/img/homebackground.jpg'
import {Category, Product as ProductType} from '../../assets/types/types'

interface HomePageProps {
  categories: Category[]
  products: ProductType[]
}

const HomePage:React.FC<HomePageProps> = ({categories, products}) => {
  return (
    <div>
      <Heading title="Bienvenue au restaurant" introduction="Découvrez nos produits" backgroundImage={HomeBackground} />
      <div className='mx-40 py-10'>
        {categories.map(({ id, title, introduction, token }: Category) => {
           const categoryProducts = products.filter(product => product.category === token).slice(0, 4)
          return(
          <section key={id}>
            <h2 className='text-tasty text-2xl'>{title}</h2>
            <span className='text-tasty text-base'>{introduction}</span>
            <div className='py-10 flex flex-wrap [&>*:nth-child(4n)]:pr-0 [&>*:nth-child(5)]:pl-0'>
              {categoryProducts.map((product:ProductType) => (
                <Product key={product.id} product={product} />
              ))}
            </div>
          </section>
          )
        })}
         
      </div>
    </div>
  );
}

export default HomePage;
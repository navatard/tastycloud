import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom';
import Cart from './Cart';
import { CartContext } from '../../context/CartContext';

// Mock data for the cart
const cart = [
  {
    id: 1,
    name: 'Test Product 1',
    description: 'This is a test product description 1',
    price: 10,
    image: 'https://via.placeholder.com/150',
    category: 'starters',
  },
  {
    id: 2,
    name: 'Test Product 2',
    description: 'This is a test product description 2',
    price: 20,
    image: 'https://via.placeholder.com/150',
    category: 'dishes',
  },
];

// Mock context values
const mockCartContext = {
  cart,
  isCartOpen: true,
  openCartPage: jest.fn(),
};

describe('Cart Component', () => {
  test('renders cart page correctly when cart is open', () => {
    render(
      <CartContext.Provider value={mockCartContext}>
        <Cart />
      </CartContext.Provider>
    );

    // Check if the title is rendered
    expect(screen.getByText('Panier')).toBeInTheDocument();

    // Check if the products are rendered correctly
    cart.forEach(product => {
      expect(screen.getByText(product.name)).toBeInTheDocument();
    });

    // Check if the close button is present
    const closeButton = screen.getByAltText('Fermer le panier');
    expect(closeButton).toBeInTheDocument();
  });

  test('calls openCartPage function when close button is clicked', () => {
    render(
      <CartContext.Provider value={mockCartContext}>
        <Cart />
      </CartContext.Provider>
    );

    const closeButton = screen.getByAltText('Fermer le panier');
    fireEvent.click(closeButton);

    expect(mockCartContext.openCartPage).toHaveBeenCalled();
  });

  test('renders empty cart message when cart is empty', () => {
    const emptyCartContext = {
      ...mockCartContext,
      cart: [],
    };

    render(
      <CartContext.Provider value={emptyCartContext}>
        <Cart />
      </CartContext.Provider>
    );

    // Check if the empty cart message is rendered
    expect(screen.getByText('Votre panier est vide')).toBeInTheDocument();
  });

  test('renders checkout button when cart has items', () => {
    render(
      <CartContext.Provider value={mockCartContext}>
        <Cart />
      </CartContext.Provider>
    );

    // Check if the checkout button is rendered
    expect(screen.getByText('Valider le panier')).toBeInTheDocument();
  });
});
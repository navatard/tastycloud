import React, { useContext } from 'react';
import categories from '../../assets/data/categoryData';
import Product from '../Product/Product';
import { CartContext } from '../../context/CartContext';
import Cross from '../../assets/img/ui/cross.png';
import {Category, Product as ProductType } from '../../assets/types/types'

const Cart: React.FC = () => {
  const context = useContext(CartContext);

  if (!context) {
    throw new Error('CartContext must be used within a CartProvider');
  }

  const { cart, isCartOpen, openCartPage } = context;
  return (
    <div className={`${isCartOpen ? "right-0" : "right-[-100vw]"} transition-all fixed h-[calc(100vh-126px)] w-1/3 z-10 top-[126px] bg-white border-l-tasty border-l-2`}>
      <div className='overflow-y-scroll px-5 h-[inherit]'>
        <div className='flex items-center justify-between'>
          <h2 className='py-3 text-2xl'>Panier</h2>
          <img className='w-6 cursor-pointer' src={Cross} alt='Fermer le panier' onClick={() => openCartPage()} />
        </div>
      <div>
        {cart.length === 0 ? (
          <p>Votre panier est vide</p>
        ) : (
          categories.map((category: Category) => {
            const categoryProducts = cart.filter((product: ProductType) => product.category === category.token).slice(0, 4)
            if(categoryProducts.length > 0) {
              return(
                <section key={category.id}>
                  <h3 className='text-tasty text-base'>{category.title}</h3>
                  <div className='flex flex-col px-10'>
                    {categoryProducts.map((product: ProductType) => (
                      <Product key={product.id} product={product} isInCart />
                    ))}
                  </div>
                </section>
                )
            }else{
              return(
                <section key={category.id}>
                  <h3 className='text-tasty text-base'>{category.title}</h3>
                  <div className='py-3 text-sm'>Pas d'articles</div>
                </section>
              )
            }
           
         })
        )}
        {cart.length !== 0 && 
        <div className='text-center pb-5'>
          <button className='py-2 px-4 bg-tasty text-white text-base rounded-md mt-5'>Valider le panier</button>
        </div>
        }
      </div>
      </div>
    </div>
  );
}

export default Cart;
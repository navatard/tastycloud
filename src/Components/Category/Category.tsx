import React from 'react';
import Product from '../Product/Product';
import Heading from '../ui/Heading/Heading';
import {Category as CategoryType, Product as ProductType} from '../../assets/types/types'

interface CategoryProps {
  category: CategoryType
  products: ProductType[]
}

const Category: React.FC<CategoryProps> = ({ category, products }) => {
    
  // Filtrer les produits par la catégorie trouvée
  const filteredProducts = products.filter((product: ProductType) => product.category=== category.token);

  return (
    <div>
      <Heading title={category.title} introduction={category.introduction} backgroundImage={category.image} />
      <div className='mx-40 py-10'>
        <div className='py-10 flex flex-wrap [&>*:nth-child(4n)]:pr-0 [&>*:nth-child(5)]:pl-0'>
            {filteredProducts.map((product: ProductType) => (
              <Product key={product.id} product={product} />
            ))}
        </div>
      </div>
    </div>
  );
}

export default Category;
import React from 'react';

interface HeadingProps {
  title: string;
  introduction: string;
  backgroundImage: string;
}

const Heading:React.FC<HeadingProps> = ({title, introduction, backgroundImage}) => {
  return (
    <div className='relative'>
        <div className='absolute size-full top-0 bg-no-repeat bg-center bg-cover' style={{backgroundImage: `url(${backgroundImage})`}}></div>
        <div className='absolute size-full top-0 bg-black/35'></div>
        <div className='relative mx-40 py-20'>
            <h1 className='text-white text-3xl'>{title}</h1>
            <p className='text-white text-lg'>{introduction}</p>
        </div>
    </div>
  );
}

export default Heading;
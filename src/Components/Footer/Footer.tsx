import Logo from '../../assets/img/logo.jpg'

const Footer = () => {
    return(
        <div className="h-44 w-full bg-tasty flex items-center justify-center">
            <img className="w-44" src={Logo} alt="Footer logo" />
        </div>
    )
}

export default Footer;